import logging
import os
import git
import requests

logging.basicConfig(level=logging.INFO)

CI_API_V4_URL = os.environ["CI_API_V4_URL"]
CI_PROJECT_ID = os.environ["CI_PROJECT_ID"]

try:
    requests_args = {"headers": {"PRIVATE-TOKEN": os.environ["PRIVATE_TOKEN"]}}
except KeyError:
    requests_args = {"headers": {"JOB-TOKEN": os.environ["CI_JOB_TOKEN"]}}


class RequestFailed(Exception):
    pass


def gitlab_get(relative_url):
    r = requests.get(f"{CI_API_V4_URL}{relative_url}", **requests_args)
    if r.status_code != 200:
        raise (RequestFailed(r.status_code))
    return r.json()


def gitlab_delete(relative_url):
    r = requests.delete(f"{CI_API_V4_URL}{relative_url}", **requests_args)
    if r.status_code != 200:
        raise (RequestFailed(r.status_code))
    return r.json()


def get_item_by_name(l, name):
    return next(item for item in l if item["name"] == name)


def set_of_names(l):
    return {item["name"] for item in l}


def clean():
    repositories = gitlab_get(
        f"/projects/{CI_PROJECT_ID}/registry/repositories/?tags=true"
    )

    try:
        repository_ci = get_item_by_name(repositories, "ci")
    except StopIteration:
        return

    repository_id = repository_ci["id"]
    tag_names = set_of_names(repository_ci["tags"])

    repo = git.Repo(".")
    remote = repo.remote()
    remote.fetch()

    for ref in remote.refs:
        try:
            tag_names.remove(ref.remote_head)
            logging.info(f"Keep used tag {ref.remote_head}")
        except KeyError:
            logging.info(f"Unknown branch {ref.remote_head}")
            pass

    for dead_tag in tag_names:
        logging.info(f"Delete dead tag {dead_tag}")
        gitlab_delete(
            f"/projects/{CI_PROJECT_ID}/registry/repositories/{repository_id}/tags/{dead_tag}"
        )


clean()
