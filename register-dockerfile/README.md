# Template for registering `Dockerfile`-generated containers on GitLab Container Registry

This directory provides a template
[`register-dockerfile.yml`](register-dockerfile.yml)
that can be included in GitLab pipeline specifications
(`.gitlab-ci.yml` files) to automate container building and
registration on GitLab Container Registry.

You may add the following lines to include this template in your
pipeline specification.
```yaml
include:
  - project: "inria-ci/docker"
    file: "/register-dockerfile/register-dockerfile.yml"
```

This template defines two
[hidden jobs](https://docs.gitlab.com/ee/ci/jobs/index.html#hide-jobs)
that you can extend in your pipeline:
- `.register-dockerfile-prepare` builds the container and pushes its
  image on GitLab Container registry;
- `.register-dockerfile-clean` removes container images whose tags do
  no longer correspond to any branches (this is optional, but keeps
  the repository clean).

The following job extends `.register-dockerfile-prepare` and specifies
that the job should run when `Dockerfile` changes.

```yaml
prepare:
  rules:
    - changes:
        - Dockerfile
  extends: .register-dockerfile-prepare
```

By default, this job runs on [`.pre` stage](https://docs.gitlab.com/ee/ci/yaml/#stage-pre),
builds the `Dockerfile` present at the root of the repository,
logs on GitLab using the job token
and pushes the image with the name `ci` and by using the name of the branch as tag name.

The following variables allow the extending job to customize this behavior.
- `$image_name` sets the name of the image, `ci` by default.
- `$image_tag` sets the tag of the image,
  `$CI_COMMIT_BRANCH$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME`
  by default (only one of these two variables is
  [predefined](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)
  for a given pipeline.
  If the pipeline is associated to a merge request,
  `CI_COMMIT_BRANCH` is empty and `$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME`
  is the name of the source branch.
  Otherwise, `CI_COMMIT_BRANCH` is the name of the commit branch and
  `$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME` is empty.
- `$image` is the qualified name of the image,
  `$CI_REGISTRY_IMAGE/$image_name:$image_tag` by default.
- `$docker_build_root` is the root path for the build relatively to
  the root of the repository, `.` by default.
- `$dockerfile` is the name of the `Dockerfile`,
  `$docker_build_root/Dockerfile` by default.

If the image depends on other files in the repository, these files should be
listed under
[`rules:changes`](https://docs.gitlab.com/ee/ci/yaml/#ruleschanges) key.

Then, jobs just have to contain the following key to use the resulting image.
```yaml
job:
  image: "$image"
```

It is worth noticing that the images are tagged with branch names:
this allows branches to run pipelines with distinct respective
environments, without overriding each other images. Indeed, distinct
environments should have distinct tags: otherwise, we could have the
following bad scenario:
- branch `A` commits its environment in a common image `ci:latest`;
- branch `B` commits another (incompatible) environment in a common
  image `ci:latest`;
- a new commit arrives to branch `A` without changing `Dockerfile`:
  the environment is not built again, so the pipeline is executed with
  the incompatible image `ci:latest` built for branch `B`.

This bad scenario is demonstrated in
[this build](https://gitlab.inria.fr/gitlabci_gallery/latex/latex-beamer/-/jobs/2934771):
this build performed on a branch `test-branch-with-minted` fails
because another branch `test-branch-without-minted` has changed the
container in an incompatible way, but the docker container has not
been rebuilt when the last commit in `test-branch-with-minted
arrived`, since this commit does not change the `Dockerfile` with
respect to previous commits in the same branch.

Ideally, the tag should uniquely identify the `Dockerfile` that
generated the image (typically by using a hash), but running jobs on
images whose tag is computed by a hash would require a
[dynamic child pipeline](https://docs.gitlab.com/ee/ci/pipelines/downstream_pipelines.html#dynamic-child-pipelines).
Using the branch name is a safe approximation (we build more images
than necessary, since several branches could share the same
environment).

Image tags that do no longer correspond to any branch should be
removed (since their corresponding branch has been removed). The
following job does that automatically on
[`.post` stage](https://docs.gitlab.com/ee/ci/yaml/#stage-post), and is
[always](https://docs.gitlab.com/ee/ci/yaml/#when) run (even if a
previous job in the pipeline has failed).

```yaml
clean:
  extends: .register-dockerfile-clean
```
This job requires access repository tags: currently, the default
`CI_JOB_TOKEN` provided by GitLab gives access repository tags
only if
[`ci_job_token_scope` feature flag](https://docs.gitlab.com/ee/ci/jobs/ci_job_token.html)
is enabled, which is the case on gitlab.inria.fr and
gitlab-int.inria.fr.
Alternatively, if the CI variable `PRIVATE_TOKEN` is set, then
it will subsume `CI_JOB_TOKEN` (in this case, the value of
`PRIVATE_TOKEN` should be an
[access token](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html#create-a-project-access-token)
with `api` permissions).
