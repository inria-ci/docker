set -ex
export DEBIAN_FRONTEND=noninteractive
cat >/etc/apt/apt.conf.d/90forceyes <<EOF
APT::Get::Assume-Yes "true";
EOF
apt-get update
apt-get install software-properties-common curl wget git sudo
adduser --disabled-password --gecos ci --shell /bin/bash ci
echo ci ALL=\(ALL\) NOPASSWD:ALL >/etc/sudoers.d/99-ci
add-apt-repository ppa:deadsnakes/ppa
apt-get update
apt-get install python3.11 python3-pip
update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.11 1
