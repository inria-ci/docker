# Docker container with Python 3.11 and packages to access CloudStack and GitLab APIs

The [Dockerfile](Dockerfile) specifies a Docker container
based on Ubuntu 22.04, with Python 3.11 and the
packages [cs](https://pypi.org/project/cs/),
[python-gitlab](https://pypi.org/project/python-gitlab/) and
[GitPython](https://pypi.org/project/GitPython/).
