set -ex
export DEBIAN_FRONTEND=noninteractive
cat >/etc/apt/apt.conf.d/90forceyes <<EOF
APT::Get::Assume-Yes "true";
EOF
apt-get update
apt-get install software-properties-common
add-apt-repository ppa:deadsnakes/ppa
apt-get update
apt-get install python3.11 python3-pip
update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.11 1
