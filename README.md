# Resources for docker containers and shared runners

This repository contains docker containers and scripts that can be u
seful for continuous integration in GitLab, in particular with shared runners.

- [python3-gitlabci/](python3-gitlabci/): this directory
contains the specification of the
[docker container](https://gitlab.inria.fr/inria-ci/docker/container_registry/2120)
`registry.gitlab.inria.fr/inria-ci/docker/python3-gitlabci`,
providing Python 3.11 and packages to access CloudStack and GitLab APIs.

- [register-dockerfile/](register-dockerfile/):
this directory provides a template
[`register-dockerfile.yml`](register-dockerfile/register-dockerfile.yml)
that can be included in GitLab pipeline specifications
(`.gitlab-ci.yml` files) to automate container building and
registration on GitLab Container Registry.

In addition, the container registry contains
[mirrors for ubuntu images](https://gitlab.inria.fr/inria-ci/docker/container_registry/2080)
as
`registry.gitlab.inria.fr/inria-ci/docker/python3-gitlabci`
with tags `latest`, `22.10`, `22.04` and `20.04`.
